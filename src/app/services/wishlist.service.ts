import { Injectable } from '@angular/core';
import { List } from '../models/list.model';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  lists: List[] = [];

  constructor() {
    this.loadStorage();
  }

  addList(title: string) {
    const newList = new List(title);
    this.lists.push(newList);
    this.saveStorage();
    return newList.id;
  }

  obtainList(id: string | number) {
    id = Number(id);
    return this.lists.find(dataList => {
      return dataList.id === id;
    });
  }

  deleteList(list: List) {
    this.lists = this.lists.filter(listData => list.id != listData.id);
    this.saveStorage();
  }

  saveStorage() {
    localStorage.setItem('data', JSON.stringify(this.lists));
  }

  loadStorage() {
    const dataFromLocalStorage = localStorage.getItem('data');
    if (dataFromLocalStorage) {
      this.lists = JSON.parse(dataFromLocalStorage);
    }
  }
}
