import { ItemList } from "./item-list.model";

export class List {
    id: number;
    title: string;
    createdAt: Date;
    finishedAt: Date;
    isFinished: boolean;
    items: ItemList[];

    constructor(title: string ) {
        this.title = title;
        this.createdAt = new Date();
        this.isFinished = false;
        this.items = [];
        this.id = new Date().getTime();

    }
}