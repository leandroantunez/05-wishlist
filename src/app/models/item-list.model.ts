export class ItemList {
    desc: string;
    isFinished: boolean;

    constructor(desc: string) {
        this.desc = desc;
        this.isFinished = false;
    }
}