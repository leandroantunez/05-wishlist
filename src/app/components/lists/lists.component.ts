import { Component, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList } from '@ionic/angular';
import { List } from 'src/app/models/list.model';
import { WishlistService } from 'src/app/services/wishlist.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html'
})
export class ListsComponent {

  @ViewChild(IonList) list: IonList;
  @Input() lists: any[];
  @Input() finished = true;

  constructor(
    private router: Router,
    public wishlistService: WishlistService,
    private alertController: AlertController
  ) {
    this.lists = wishlistService.lists;
  }

  selectedList(list: List) {
    if (this.finished) {
      this.router.navigateByUrl(`/tabs/tab2/add/${list.id}`)
    } else {
      this.router.navigateByUrl(`/tabs/tab1/add/${list.id}`)
    }
  }

  deleteList(list: List) {
    this.wishlistService.deleteList(list);
  }

  async editList(list: List) {
    const alert = await this.alertController.create
      ({
        header: 'Edit list',
        inputs: [{
          name: 'title',
          type: 'text',
          value: list.title,
          placeholder: 'List name'
        }],
        buttons:
          [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel');
              }
            },
            {
              text: 'Update',
              handler: (data) => {
                if (data.title.length === 0) {
                  return;
                }
                list.title = data.title;
                this.wishlistService.saveStorage();
                this.list.closeSlidingItems();
              }
            }
          ]
      })
    alert.present();
  }

}