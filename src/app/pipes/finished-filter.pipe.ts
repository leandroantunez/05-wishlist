import { Pipe, PipeTransform } from '@angular/core';
import { List } from '../models/list.model';

@Pipe({
  name: 'finishedFilter',
  pure: false
})
export class FinishedFilterPipe implements PipeTransform {

  transform(lists: List[], finished: boolean = true): List[] {
    return lists.filter(list => list.isFinished === finished)
  }

}
