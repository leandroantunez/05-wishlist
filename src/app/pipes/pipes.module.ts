import { NgModule } from '@angular/core';
import { FinishedFilterPipe } from './finished-filter.pipe';

@NgModule({
  declarations: [
    FinishedFilterPipe
  ],
  imports: [],
  exports: [FinishedFilterPipe]
})
export class PipesModule { }
