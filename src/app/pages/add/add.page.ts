import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ItemList } from 'src/app/models/item-list.model';
import { List } from 'src/app/models/list.model';
import { WishlistService } from 'src/app/services/wishlist.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  list: List;
  itemName = '';

  constructor(
    private wishlist: WishlistService,
    private route: ActivatedRoute
  ) {
    const listId = this.route.snapshot.paramMap.get('listId');
    this.list = this.wishlist.obtainList(listId);
  }

  ngOnInit() {
  }

  addItem() {
    if (this.itemName.length === 0) {
      return;
    }
    const newItem = new ItemList(this.itemName);
    this.list.items.push(newItem);

    this.itemName = '';
  }

  changeCheck(item: ItemList) {
    const pendings = this.list.items
      .filter(item => !item.isFinished)
      .length;
    if (pendings === 0) {
      this.list.finishedAt = new Date();
      this.list.isFinished = true;
    } else {
      this.list.finishedAt = null;
      this.list.isFinished = false;
    }
    this.wishlist.saveStorage();
  }

  delete(i: number) {
    this.list.items.splice(i, 1);
    this.wishlist.saveStorage();
  }
}
