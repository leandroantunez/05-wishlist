import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { List } from 'src/app/models/list.model';
import { WishlistService } from 'src/app/services/wishlist.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  lists: List[];

  constructor(
    public wishlist: WishlistService,
    private router: Router,
    private alertController: AlertController
  ) {
    this.lists = this.wishlist.lists;
  }

  async addList() {
    const alert = await this.alertController.create
      ({
        header: 'New list',
        inputs: [{
          name: 'title',
          type: 'text',
          placeholder: 'List name'
        }],
        buttons:
          [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel');
              }
            },
            {
              text: 'Add',
              handler: (data) => {
                if (data.title.length === 0) {
                  return;
                }
                const listId = this.wishlist.addList(data.title);
                this.router.navigateByUrl(`/tabs/tab1/add/${listId}`)
              }
            }
          ]
      })
      alert.present();
  }

}
